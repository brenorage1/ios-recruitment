import Foundation

enum ExchangeRatesApiEndpoint: EndpointProtocol {

    case tradeCurrencies(base: CurrencyType, toTrade: CurrencyType)

    var scheme: EndpointScheme {
        switch self {
        case .tradeCurrencies:
            return .https
        }
    }

    var httpMethod: HTTPMethod {
        switch self {
        case .tradeCurrencies:
            return .get
        }
    }

    var host: String {
        switch self {
        case .tradeCurrencies:
            return "api.exchangeratesapi.io"
        }
    }

    var path: String {
        switch self {
        case .tradeCurrencies:
            return "/latest"
        }
    }

    var parameters: [URLQueryItem] {
        switch self {
        case let .tradeCurrencies(base, toTrade):
            let symbols = URLQueryItem(name: "symbols", value: toTrade.rawValue)
            let base = URLQueryItem(name: "base", value: base.rawValue)
            return [base, symbols]
        }
    }
}
