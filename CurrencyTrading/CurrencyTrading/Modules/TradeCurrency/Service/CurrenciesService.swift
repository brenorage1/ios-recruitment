import Foundation

protocol CurrenciesServiceProtocol {
    func tradeCurrencies(base: CurrencyType, toTrade: CurrencyType, completion: @escaping RequestCallback<LatestCurrenciesModel>)
}

struct CurrenciesService: CurrenciesServiceProtocol {
    private let httpService: HTTPServicesProtocol

    init(httpService: HTTPServicesProtocol = HTTPServices()) {
        self.httpService = httpService
    }

    func tradeCurrencies(base: CurrencyType, toTrade: CurrencyType, completion: @escaping RequestCallback<LatestCurrenciesModel>) {
        let endpoint = ExchangeRatesApiEndpoint.tradeCurrencies(base: base, toTrade: toTrade)
        httpService.request(endpoint: endpoint) { (result: Result<LatestCurrenciesModel, NetworkError>) in
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
}
