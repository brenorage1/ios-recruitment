import UIKit

final class SelectCurrencyController: UIViewController {

    typealias View = SelectCurrencyViewProtocol & UIView

    private var selectCurrencyView: View
    private let currenciesService: CurrenciesServiceProtocol

    var didFinishSelectCurrency: ((LatestCurrenciesModel) -> Void)?

    init(selectCurrencyView: View = SelectCurrencyView(),
         currenciesService: CurrenciesServiceProtocol = CurrenciesService()) {
        self.selectCurrencyView = selectCurrencyView
        self.currenciesService = currenciesService
        super.init(nibName: nil, bundle: nil)
        title = Strings.SelectCurrency.title
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func loadView() {
        self.view = selectCurrencyView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        selectCurrencyView.setPlaceholder(with: Strings.SelectCurrency.BaseCurrecyPicker.placeholder,
                                          and: Strings.SelectCurrency.CurrencyToTradePicker.placeholder)
        selectCurrencyView.setupPickers(with: CurrencyType.allCases)

        selectCurrencyView
            .didTapTradeButton = { [weak self] baseCurrency, currencyToTrade in
            self?.tradeCurrency(base: baseCurrency, toTrade: currencyToTrade)
        }
    }

    private func tradeCurrency(base: CurrencyType, toTrade: CurrencyType) {
        selectCurrencyView.startLoading()
        currenciesService.tradeCurrencies(base: base, toTrade: toTrade) { [weak self] result in
            self?.selectCurrencyView.stopLoading()
            switch result {
            case let .success(model):
                self?.didFinishSelectCurrency?(model)
            case let .failure(error):
                self?.showError(error.title, error.message)
            }
        }
    }

    private func showError(_ title: String, _ message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: Strings.SelectCurrency.Error.button, style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}
