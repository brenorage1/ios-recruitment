import UIKit

protocol SelectCurrencyViewProtocol {
    var didTapTradeButton: ((CurrencyType, CurrencyType) -> Void)? { get set }

    func setPlaceholder(with baseCurrencyPlaceholder: String, and currencyToChangePlaceholder: String)
    func setupPickers(with currencies: [CurrencyType])
    func startLoading()
    func stopLoading()
}

final class SelectCurrencyView: UIView {
    private let baseCurrencyPicker: PickerTextField = {
        let picker = PickerTextField()
        picker.textColor = ColorName.primary.color
        picker.backgroundColor = .white
        picker.layer.borderColor = ColorName.primary.color.cgColor
        picker.layer.borderWidth = 1.0
        picker.layer.cornerRadius = 8
        picker.accessibilityIdentifier = "baseCurrencyPicker"
        return picker
    }()

    private let currencyToTradePicker: PickerTextField = {
        let picker = PickerTextField()
        picker.textColor = ColorName.primary.color
        picker.backgroundColor = .white
        picker.layer.borderColor = ColorName.primary.color.cgColor
        picker.layer.borderWidth = 1.0
        picker.layer.cornerRadius = 8
        picker.accessibilityIdentifier = "currencyToTradePicker"
        return picker
    }()

    private lazy var tradeCurrencyButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .white
        button.setTitleColor(ColorName.primary.color, for: .normal)
        button.setTitleColor(.lightGray, for: .disabled)
        button.setTitle(Strings.SelectCurrency.Button.title, for: .normal)
        button.isEnabled = false
        button.addTarget(self, action: #selector(tradeButtonAction), for: .touchUpInside)
        button.accessibilityIdentifier = "tradeCurrencyButton"
        return button
    }()

    private let loadingView: UIActivityIndicatorView = {
        let loading = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.medium)
        loading.color = .gray
        loading.isHidden = true
        loading.accessibilityIdentifier = "loadingView"
        return loading
    }()

    private var baseCurrency: CurrencyType?
    private var currencyToTrade: CurrencyType?

    var didTapTradeButton: ((CurrencyType, CurrencyType) -> Void)?

    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        setupView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension SelectCurrencyView: SelectCurrencyViewProtocol {
    func setPlaceholder(with baseCurrencyPlaceholder: String, and currencyToChangePlaceholder: String) {
        baseCurrencyPicker.placeholder = baseCurrencyPlaceholder
        currencyToTradePicker.placeholder = currencyToChangePlaceholder
    }

    func setupPickers(with currencies: [CurrencyType]) {
        baseCurrencyPicker.currencies = currencies
        currencyToTradePicker.currencies = currencies

        baseCurrencyPicker.didSelectItem = {
            self.baseCurrency = $0
            self.setupButtonState()
        }

        currencyToTradePicker.didSelectItem = {
            self.currencyToTrade = $0
            self.setupButtonState()
        }
    }

    func startLoading() {
        UIView.animate(withDuration: 0.3) {
            DispatchQueue.main.async {
                self.tradeCurrencyButton.isEnabled = false
                self.tradeCurrencyButton.setTitle(" ", for: .disabled)
                self.loadingView.isHidden = false
                self.loadingView.startAnimating()
            }
        }
    }

    func stopLoading() {
        UIView.animate(withDuration: 0.3) {
            DispatchQueue.main.async {
                self.loadingView.isHidden = true
                self.loadingView.startAnimating()
                self.tradeCurrencyButton.isEnabled = true
                self.tradeCurrencyButton.setTitle(Strings.SelectCurrency.Button.title, for: .normal)
            }
        }
    }

    private func setupButtonState() {
        if  baseCurrency != nil,
            currencyToTrade != nil {
            tradeCurrencyButton.isEnabled = true
        } else {
            tradeCurrencyButton.isEnabled = false
        }
    }

    @objc private func tradeButtonAction() {
        guard
            let baseCurrency = baseCurrency,
            let currencyToTrade = currencyToTrade
        else { return }

        didTapTradeButton?(baseCurrency, currencyToTrade)
    }
}

extension SelectCurrencyView: ViewCodeProtocol {
    func addSubviews() {
        addSubview(baseCurrencyPicker)
        addSubview(currencyToTradePicker)
        addSubview(tradeCurrencyButton)
        addSubview(loadingView)
    }

    func setConstraints() {
        baseCurrencyPicker.layout {
            $0.top == layoutMarginsGuide.topAnchor + 20
            $0.leading == leadingAnchor + 16
            $0.trailing == trailingAnchor - 16
            $0.height == 50
        }

        currencyToTradePicker.layout {
            $0.top == baseCurrencyPicker.bottomAnchor + 16
            $0.leading == leadingAnchor + 16
            $0.trailing == trailingAnchor - 16
            $0.height == 50
        }

        tradeCurrencyButton.layout {
            $0.top == currencyToTradePicker.bottomAnchor + 16
            $0.leading == leadingAnchor + 16
            $0.trailing == trailingAnchor - 16
            $0.height == 50
        }

        loadingView.layout {
            $0.centerXAnchor == tradeCurrencyButton.centerXAnchor
            $0.centerYAnchor == tradeCurrencyButton.centerYAnchor
        }
    }

    func configViews() {
        backgroundColor = ColorName.primary.color
        currencyToTradePicker.frame.inset(by: .init(top: 0, left: 8, bottom: 0, right: 8))
        baseCurrencyPicker.frame.inset(by: .init(top: 0, left: 8, bottom: 0, right: 8))
        tradeCurrencyButton.layer.cornerRadius = 8
    }
}
