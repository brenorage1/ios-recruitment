import Foundation

struct LatestCurrenciesModel: Codable {

    var currencyToTrade: CurrencyType
    var tradeMultiplier: Double
    let base: CurrencyType
    private let rates: [String: Float]

    init(from decoder: Decoder) throws {
        self.base = try decoder.container(keyedBy: CodingKeys.self).decode(CurrencyType.self, forKey: .base)
        self.rates = [:]
        self.currencyToTrade = .unknown
        self.tradeMultiplier = 0.00

        let ratesDict = try decoder.container(keyedBy: CodingKeys.self).decode([String: Double].self, forKey: .rates)
        ratesDict.forEach {
            self.currencyToTrade = CurrencyType(rawValue: $0.key) ?? .unknown
            self.tradeMultiplier = $0.value
        }
    }
}
