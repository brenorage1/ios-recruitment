import UIKit

final class TradeCurrencyCoordinator: CoordinatorProtocol {
    var childCoordinators: [CoordinatorProtocol] = []

    var navigationController: UINavigationController

    init(navigationController: UINavigationController = UINavigationController()) {
        self.navigationController = navigationController
    }

    func start() {
        goToSelectCurrencyController()
    }

    private func goToSelectCurrencyController() {
        let controller = SelectCurrencyController()

        controller.didFinishSelectCurrency = { [weak self] latestCurrencies in
            self?.goToTradeCurrency(latestCurrencies: latestCurrencies)
        }

        navigationController.pushViewController(controller, animated: true)
    }

    private func goToTradeCurrency(latestCurrencies: LatestCurrenciesModel) {
        let controller = TradeCurrencyController(latestCurrencies: latestCurrencies)
        navigationController.pushViewController(controller, animated: true)
    }
}
