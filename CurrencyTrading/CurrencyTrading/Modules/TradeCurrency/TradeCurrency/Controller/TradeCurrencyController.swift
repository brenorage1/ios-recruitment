import UIKit

final class TradeCurrencyController: UIViewController {
    typealias View = TradeCurrencyViewProtocol & UIView

    private var tradeCurrencyView: View
    private let latestCurrencies: LatestCurrenciesModel

    init(tradeCurrencyView: View = TradeCurrencyView(),
         latestCurrencies: LatestCurrenciesModel) {
        self.tradeCurrencyView = tradeCurrencyView
        self.latestCurrencies = latestCurrencies
        super.init(nibName: nil, bundle: nil)
        setupView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func loadView() {
        self.view = tradeCurrencyView
    }

    private func setupView() {
        tradeCurrencyView.baseCurrency = latestCurrencies.base
        tradeCurrencyView.setBaseCurrency(name: latestCurrencies.base.currencyName)
        tradeCurrencyView.setCurrencyToTrade(name: latestCurrencies.currencyToTrade.currencyName)
        tradeCurrencyView.setCurrencyTradedValue(with: "000".getFormatted(by: latestCurrencies.currencyToTrade) ?? "")

        tradeCurrencyView.didTapTradeButton = { [weak self] currencyValue in self?.tradeCurrency(with: currencyValue) }
    }

    private func tradeCurrency(with value: String) {
        guard let valueNumber = Double(value.getOnlyDigits()?.getCurrencyWithCents() ?? "") else { return }

        var valueTraded = (valueNumber * latestCurrencies.tradeMultiplier)
        valueTraded = (valueTraded * 100).rounded() / 100
        tradeCurrencyView.setCurrencyTradedValue(with: "\(valueTraded)".getOnlyDigits()?.getCurrencyWithCents().getFormatted(by: latestCurrencies.currencyToTrade) ?? "unknown")
    }
}
