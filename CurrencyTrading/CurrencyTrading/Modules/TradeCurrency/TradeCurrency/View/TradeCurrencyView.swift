import UIKit

protocol TradeCurrencyViewProtocol {
    var baseCurrency: CurrencyType? { get set }
    var didTapTradeButton: ((String) -> Void)? { get set }

    func setBaseCurrency(name: String)
    func setCurrencyToTrade(name: String)
    func setCurrencyTradedValue(with value: String)
}

final class TradeCurrencyView: UIView {
    private let baseCurrencyLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.textColor = .white
        label.accessibilityIdentifier = "baseCurrencyLabel"
        return label
    }()

    private let inputCurrencyValueField: CustomTextField = {
        let textField = CustomTextField()
        textField.placeholder = Strings.TradeCurrency.InputValue.placeholder
        textField.tintColor = ColorName.primary.color
        textField.textAlignment = .center
        textField.backgroundColor = .white
        textField.keyboardType = .numberPad
        textField.accessibilityIdentifier = "inputCurrencyValueField"
        return textField
    }()

    private lazy var tradeButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.TradeCurrency.TradeButton.title, for: .normal)
        button.setTitleColor(ColorName.primary.color, for: .normal)
        button.addTarget(self, action: #selector(tradeAction), for: .touchUpInside)
        button.backgroundColor = .white
        button.accessibilityIdentifier = "tradeButton"
        return button
    }()

    private let currencyToTradeLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.textColor = .white
        label.accessibilityIdentifier = "currencyToTradeLabel"
        return label
    }()

    private let currencyToTradeValueLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.backgroundColor = .white
        label.accessibilityIdentifier = "currencyToTradeValueLabel"
        return label
    }()

    var baseCurrency: CurrencyType? {
        didSet {
            guard let baseCurrency = baseCurrency else { return }
            inputCurrencyValueField.type = .currency(type: baseCurrency)
        }
    }
    var didTapTradeButton: ((String) -> Void)?

    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        setupView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @objc private func tradeAction() {
        guard let text = inputCurrencyValueField.text else { return }
        didTapTradeButton?(text)
    }

    private func setupButtonState() {
        if  inputCurrencyValueField.text != nil {
            tradeButton.isEnabled = true
        } else {
            tradeButton.isEnabled = false
        }
    }
}

extension TradeCurrencyView: TradeCurrencyViewProtocol {
    func setBaseCurrency(name: String) {
        baseCurrencyLabel.text = name
    }

    func setCurrencyToTrade(name: String) {
        currencyToTradeLabel.text = name
    }

    func setCurrencyTradedValue(with value: String) {
        currencyToTradeValueLabel.text = value
    }
}

extension TradeCurrencyView: ViewCodeProtocol {
    func addSubviews() {
        addSubview(baseCurrencyLabel)
        addSubview(inputCurrencyValueField)
        addSubview(tradeButton)
        addSubview(currencyToTradeLabel)
        addSubview(currencyToTradeValueLabel)
    }

    func setConstraints() {
        baseCurrencyLabel.layout {
            $0.top == layoutMarginsGuide.topAnchor + 40
            $0.leading == leadingAnchor + 16
            $0.trailing == trailingAnchor - 16
        }

        inputCurrencyValueField.layout {
            $0.top == self.baseCurrencyLabel.bottomAnchor + 16
            $0.leading == leadingAnchor + 16
            $0.trailing == trailingAnchor - 16
            $0.height == 50
        }

        tradeButton.layout {
            $0.top == inputCurrencyValueField.bottomAnchor + 16
            $0.leading == leadingAnchor + 16
            $0.trailing == trailingAnchor - 16
            $0.height == 50
        }

        currencyToTradeLabel.layout {
            $0.top == tradeButton.bottomAnchor + 16
            $0.leading == leadingAnchor + 16
            $0.trailing == trailingAnchor - 16
        }

        currencyToTradeValueLabel.layout {
            $0.top == currencyToTradeLabel.bottomAnchor + 16
            $0.leading == leadingAnchor + 16
            $0.trailing == trailingAnchor - 16
            $0.height == 50
        }
    }

    func configViews() {
        backgroundColor = ColorName.primary.color
        tradeButton.layer.cornerRadius = 8
        inputCurrencyValueField.layer.cornerRadius = 8
        inputCurrencyValueField.didChanged = { self.setupButtonState() }
        currencyToTradeValueLabel.layer.cornerRadius = 8
    }
}
