import UIKit

final class PickerTextField: UITextField {

    private lazy var pickerView: UIPickerView = {
        let picker = UIPickerView()
        picker.delegate = self
        picker.dataSource = self
        return picker
    }()

    private lazy var toolbar: UIToolbar = {
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: pickerView.frame.width, height: 44))
        toolbar.barStyle = .default
        toolbar.isTranslucent = false
        toolbar.sizeToFit()
        toolbar.barTintColor = .white

        let doneButton = UIBarButtonItem(title: "Pronto", style: .plain, target: self, action: #selector(doneAction))
        doneButton.accessibilityIdentifier = "donePickerButton"

        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)

        toolbar.items = [flexibleSpace, doneButton]
        return toolbar
    }()

    var currencies: [CurrencyType] = []
    var didSelectItem: ((CurrencyType) -> Void)?

    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        inputAccessoryView = toolbar
        inputView = pickerView
        self.addTarget(self, action: #selector(selectItem), for: .editingDidBegin)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @objc private func selectItem() {
        let item = currencies[pickerView.selectedRow(inComponent: 0)]
        self.text = item.currencyName
        didSelectItem?(item)
    }

    @objc private func doneAction() {
        selectItem()
        resignFirstResponder()
    }
}

extension PickerTextField: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return currencies.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return currencies[row].currencyName
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let item = currencies[row]
        self.text = item.currencyName
        didSelectItem?(item)
    }

    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: .init(top: 0, left: 8, bottom: 0, right: 8))
    }

    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: .init(top: 0, left: 8, bottom: 0, right: 8))
    }

    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: .init(top: 0, left: 8, bottom: 0, right: 8))
    }
}
