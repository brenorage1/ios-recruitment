import Foundation

extension String {
    func getOnlyDigits() -> String? {
        guard
            let regex = try? NSRegularExpression(pattern: "[^0-9]", options: .caseInsensitive)
        else { return nil }
        return regex.stringByReplacingMatches(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, count), withTemplate: "")
    }

    func getFormatted(by currencyType: CurrencyType) -> String? {
        let formatter = NumberFormatter()
        let localeID = Locale.identifier(fromComponents: [NSLocale.Key.currencyCode.rawValue : currencyType.rawValue])
        formatter.locale = Locale(identifier: localeID)
        formatter.numberStyle = .currency
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        formatter.minimumIntegerDigits = 1

        return formatter.string(from: Double(self) as NSNumber? ?? 00.00)
    }

    func getCurrencyWithCents() -> String {
        var newString = ""
        if self.count == 1 {
            newString = "0.0\(self)"
        } else if self.count == 2 {
            newString = "0.\(self)"
        } else if count > 2 {
            newString = "\(self.prefix(count - 2)).\(self.suffix(2))"
        }

        return newString
    }
}
