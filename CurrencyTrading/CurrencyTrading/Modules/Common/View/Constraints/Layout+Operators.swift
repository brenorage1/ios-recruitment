import struct CoreGraphics.CGFloat
import class UIKit.NSLayoutConstraint

// MARK: - LayoutAnchor
func + <Anchor: LayoutAnchor>(lhs: Anchor, rhs: CGFloat) -> (Anchor, CGFloat) {
    return (lhs, rhs)
}

func - <Anchor: LayoutAnchor>(lhs: Anchor, rhs: CGFloat) -> (Anchor, CGFloat) {
    return (lhs, -rhs)
}

// MARK: - LayoutProperty
@discardableResult
func == <Anchor: LayoutAnchor>(lhs: LayoutProperty<Anchor>, rhs: (anchor: Anchor, offset: CGFloat)) -> NSLayoutConstraint {
    return lhs.equal(to: rhs.anchor, offsetBy: rhs.offset)
}

@discardableResult
func == <Anchor: LayoutAnchor>(lhs: LayoutProperty<Anchor>, rhs: Anchor) -> NSLayoutConstraint {
    return lhs.equal(to: rhs)
}

@discardableResult
func >= <Anchor: LayoutAnchor>(lhs: LayoutProperty<Anchor>, rhs: (anchor: Anchor, offset: CGFloat)) -> NSLayoutConstraint {
    return lhs.greaterThanOrEqual(to: rhs.anchor, offsetBy: rhs.offset)
}

@discardableResult
func >= <Anchor: LayoutAnchor>(lhs: LayoutProperty<Anchor>, rhs: Anchor) -> NSLayoutConstraint {
    return lhs.greaterThanOrEqual(to: rhs)
}

@discardableResult
func <= <Anchor: LayoutAnchor>(lhs: LayoutProperty<Anchor>, rhs: (anchor: Anchor, offset: CGFloat)) -> NSLayoutConstraint {
    return lhs.lessThanOrEqual(to: rhs.anchor, offsetBy: rhs.offset)
}

@discardableResult
func <= <Anchor: LayoutAnchor>(lhs: LayoutProperty<Anchor>, rhs: Anchor) -> NSLayoutConstraint {
    return lhs.lessThanOrEqual(to: rhs)
}

@discardableResult
func == <Anchor: LayoutDimension>(lhs: LayoutProperty<Anchor>, rhs: CGFloat) -> NSLayoutConstraint {
    return lhs.equal(to: rhs)
}

@discardableResult
func <= <Anchor: LayoutDimension>(lhs: LayoutProperty<Anchor>, rhs: CGFloat) -> NSLayoutConstraint {
    return lhs.lessThanOrEqual(to: rhs)
}

@discardableResult
func >= <Anchor: LayoutDimension>(lhs: LayoutProperty<Anchor>, rhs: CGFloat) -> NSLayoutConstraint {
    return lhs.greaterThanOrEqual(to: rhs)
}
