protocol ViewCodeProtocol {
    func addSubviews()
    func setConstraints()
    func configViews()
}

extension ViewCodeProtocol {
    func configViews() { }

    func setupView() {
        addSubviews()
        setConstraints()
        configViews()
    }
}
