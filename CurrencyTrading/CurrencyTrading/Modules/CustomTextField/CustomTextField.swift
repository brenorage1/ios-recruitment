import UIKit

enum CustomTextFieldType {
    case currency(type: CurrencyType)
    case `default`
}

final class CustomTextField: UIView {
    private lazy var textField: UITextField = {
        let field = UITextField()
        field.addTarget(self, action: #selector(editingChange), for: .editingChanged)
        field.delegate = self
        field.backgroundColor = self.backgroundColor
        field.keyboardType = self.keyboardType
        field.textColor = self.textColor
        field.placeholder = self.placeholder
        field.isEnabled = self.isEnabled
        field.tintColor = self.tintColor
        return field
    }()

    var textAlignment: NSTextAlignment = .center {
        didSet {
            textField.textAlignment = textAlignment
        }
    }

    var keyboardType: UIKeyboardType = .default {
        didSet {
            textField.keyboardType = keyboardType
        }
    }

    var textColor: UIColor = ColorName.primary.color {
        didSet {
            textField.textColor = textColor
        }
    }

    var placeholder: String? {
        didSet {
            textField.placeholder = placeholder
        }
    }

    var isEnabled: Bool = true {
        didSet {
            textField.alpha = isEnabled ? 1 : 0.5
        }
    }

    var text: String? {
        get {
            return textField.text
        }
    }

    var type: CustomTextFieldType = .default
    var didChanged: (() -> Void)?

    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @objc private func editingChange() {
        switch type {
        case let .currency(type: currencyCode):
            guard let text = text?.getOnlyDigits()?.getCurrencyWithCents() else { return }
            self.textField.text = text.getFormatted(by: currencyCode)
        case .default:
            break
        }

        didChanged?()
    }
}

extension CustomTextField: ViewCodeProtocol {
    func addSubviews() {
        addSubview(textField)
    }

    func setConstraints() {
        textField.layout {
            $0.top == topAnchor
            $0.leading == leadingAnchor
            $0.bottom == bottomAnchor
            $0.trailing == trailingAnchor
        }
    }
}

extension CustomTextField: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch type {
        case .currency:
            guard
                let text = text as NSString?,
                let rawText = text.replacingCharacters(in: range, with: string).getOnlyDigits()?.getCurrencyWithCents()
            else { return false }

            return rawText.count <= 15
        default:
            return true
        }
    }
}
