// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name
internal enum Strings {

  internal enum Error {
    internal enum Generic {
      /// Ops, tivemos um problema em trazer os dados que voce pediu. Tente novamente.
      internal static let message = Strings.tr("Localizable", "error.generic.message")
      /// Algo deu errado
      internal static let title = Strings.tr("Localizable", "error.generic.title")
    }
    internal enum NotConnected {
      /// Parece que voce esta sem conexão. Verifique a sua rede e tente novamente.
      internal static let message = Strings.tr("Localizable", "error.notConnected.message")
      /// Sem conexão
      internal static let title = Strings.tr("Localizable", "error.notConnected.title")
    }
  }

  internal enum SelectCurrency {
    /// Selecione as moedas
    internal static let title = Strings.tr("Localizable", "selectCurrency.title")
    internal enum BaseCurrecyPicker {
      /// Selecione a sua moeda
      internal static let placeholder = Strings.tr("Localizable", "selectCurrency.baseCurrecyPicker.Placeholder")
    }
    internal enum Button {
      /// Avançar
      internal static let title = Strings.tr("Localizable", "selectCurrency.button.title")
    }
    internal enum CurrencyToTradePicker {
      /// Selecione a moeda para conversão
      internal static let placeholder = Strings.tr("Localizable", "selectCurrency.currencyToTradePicker.Placeholder")
    }
    internal enum Error {
      /// OK
      internal static let button = Strings.tr("Localizable", "selectCurrency.error.button")
    }
  }

  internal enum TradeCurrency {
    internal enum InputValue {
      /// Insira o valor que deseja trocar
      internal static let placeholder = Strings.tr("Localizable", "tradeCurrency.inputValue.placeholder")
    }
    internal enum TradeButton {
      /// Trocar
      internal static let title = Strings.tr("Localizable", "tradeCurrency.tradeButton.title")
    }
  }
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name

// MARK: - Implementation Details

extension Strings {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    // swiftlint:disable:next nslocalizedstring_key
    let format = NSLocalizedString(key, tableName: table, bundle: Bundle(for: BundleToken.self), comment: "")
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

private final class BundleToken {}
