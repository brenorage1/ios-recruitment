//
//  AppDelegate.swift
//  CurrencyTrading
//
//  Created by breno.rage.aboud on 30/11/19.
//  Copyright © 2019 breno.rage. All rights reserved.
//

import UIKit
import OHHTTPStubs

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        #if DEBUG
        if (ProcessInfo.processInfo.arguments.contains("STUB_HTTP_ENDPOINTS")) {
            OHHTTPStubs.setEnabled(true)
            stub(condition: containsQueryParams(["base": "USD"])) { request in
                return OHHTTPStubsResponse(
                    fileAtPath: OHPathForFile("LastestCurrencies.json", type(of: self))!,
                    statusCode: 200,
                    headers: ["Content-Type":"application/json"]
                )
            }

            stub(condition: containsQueryParams(["base": "JPY"])) { request in
                return OHHTTPStubsResponse(data: Data(), statusCode: 500, headers: nil)
            }
        } else {
            OHHTTPStubs.setEnabled(false)
        }
        #endif
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

