//
//  SceneDelegate.swift
//  CurrencyTrading
//
//  Created by breno.rage.aboud on 30/11/19.
//  Copyright © 2019 breno.rage. All rights reserved.
//

import UIKit
import SwiftUI

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    var appCoordinator: AppCoordinator?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        if let windowScene = scene as? UIWindowScene {
            appCoordinator = AppCoordinator()
            appCoordinator?.start()
            
            let window = UIWindow(windowScene: windowScene)
            window.rootViewController = appCoordinator?.navigationController
            self.window = window
            window.makeKeyAndVisible()
        }
    }
}

