# CurrencyTrading

Currency trading é um app para te ajudar a converter moedas do mundo inteiro.

## Instalação

Esse projeto usa Cocoapods como gerenciador de dependencias. Para usar é necessario rodar o comando abaixo.

```
pod install
```

## Ambiente

```
Xcode 11.2.1
Deployment target iOS 13.2
```

## Arquitetura
Para esse projeto escolhi usar o padrão de camada de apresentação MVC. Que consiste basicamente em uma View (UIView) e uma Controller (UIViewController), desacopladas entre si e cada uma com a sua responsabilidade a View é responsavel por exibir os itens da tela, como campo de texto e botões.  Já a Controller é responsavel por qualquer regra de negocio relativa a apresentação de dados na tela. Ainda tenho uma camada de Service que é responsavel por tratar dados e passar de maneira mais amigavel para a Controller e a camada de Coordinator que coordena os fluxos da aplicação. Para os testes usei a ferramenta da apple XCTestCase. 

## Dependencias
- SwiftGen
- OHHTTPStubs
