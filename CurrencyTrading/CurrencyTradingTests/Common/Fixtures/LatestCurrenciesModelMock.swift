import Foundation
@testable import CurrencyTrading

extension LatestCurrenciesModel {
    static func getLatestCurrenciesMockData(with bundle: Bundle) -> Data {
        let fileUrl = bundle.url(forResource: "LastestCurrencies", withExtension: "json")
        return try! Data(contentsOf: fileUrl!)
    }

    static func getLatestCurrenciesMock(with bundle: Bundle) -> LatestCurrenciesModel {
        let data = getLatestCurrenciesMockData(with: bundle)
        return try! JSONDecoder().decode(LatestCurrenciesModel.self, from: data)
    }
}
