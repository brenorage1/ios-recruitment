import XCTest
import Foundation

@testable import CurrencyTrading

final class CurrenciesServiceTest: XCTestCase {
    var sut: CurrenciesService!
    var httpServices: HTTPServices!
    var networkSession: NetworkSessionMock!

    private let successResponse = HTTPURLResponse(url: URL(string: "http://www.google.com")!, statusCode: 200, httpVersion: "HTTP/1.1", headerFields: [:])

    override func setUp() {
        networkSession = NetworkSessionMock()
        httpServices = HTTPServices(networkSession: networkSession)
        sut = CurrenciesService(httpService: httpServices)
    }

    override func tearDown() {
        sut = nil
        networkSession = nil
    }

    func testIfCurrencyModelHasCorrectlyParse() {
        networkSession.urlResponse = successResponse
        networkSession.data = LatestCurrenciesModel.getLatestCurrenciesMockData(with: Bundle(for: type(of: self)))

        let expectation = XCTestExpectation(description: "wait for model parsing")

        sut.tradeCurrencies(base: .usa, toTrade: .japan) { (result: Result<LatestCurrenciesModel, NetworkError>) in
            switch result {
            case let .success(model):
                XCTAssertTrue(model.base == .usa)
                XCTAssertTrue(model.currencyToTrade == .bulgaria)
                XCTAssertTrue(model.tradeMultiplier == 1.9)
            case .failure:
                XCTFail()
            }

            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 1.0)
    }
}
