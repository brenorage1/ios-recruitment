import XCTest
import Foundation

@testable import CurrencyTrading

final class CurrencyExtensionTests: XCTestCase {
    var sut: String!

    func testIfMillionCurrencyIsFormattedCorrectly() {
        sut = "10000000000"
        let stringFormatted = sut.getCurrencyWithCents().getFormatted(by: .usa)!
        XCTAssertEqual(stringFormatted, "US$ 100,000,000.00")
    }

    func testIfCompleteCurrencyIsFormattedCorrectly() {
        sut = "1234"
        let stringFormatted = sut.getCurrencyWithCents().getFormatted(by: .usa)!
        XCTAssertEqual(stringFormatted, "US$ 12.34")
    }

    func testIfCurrencyWithOneDigitCentIsFormattedCorrectly() {
        sut = "1"
        let stringFormatted = sut.getCurrencyWithCents().getFormatted(by: .usa)!
        XCTAssertEqual(stringFormatted, "US$ 0.01")
    }

    func testIfCurrencyWithTwoDigitsCentIsFormattedCorrectly() {
        sut = "11"
        let stringFormatted = sut.getCurrencyWithCents().getFormatted(by: .usa)!
        XCTAssertEqual(stringFormatted, "US$ 0.11")
    }
}
