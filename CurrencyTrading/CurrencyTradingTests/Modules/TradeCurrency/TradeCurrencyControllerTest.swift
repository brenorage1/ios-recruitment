import XCTest
import Foundation

@testable import CurrencyTrading

final class TradeCurrencyControllerTest: XCTestCase {
    var sut: TradeCurrencyController!
    var viewMock: TradeCurrencyViewMock!

    override func setUp() {
        viewMock = TradeCurrencyViewMock()
        sut = TradeCurrencyController(tradeCurrencyView: viewMock, latestCurrencies: LatestCurrenciesModel.getLatestCurrenciesMock(with: Bundle(for: type(of: self))))
    }

    override func tearDown() {
        sut = nil
    }

    func testIfCurrencyIsTradedCorrectly() {
        viewMock.didTapTradeButton!("US$ 12.34")
        XCTAssertEqual(viewMock.currencyTraded, "BGN 23.45")
    }
}
