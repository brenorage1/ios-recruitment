import Foundation
import UIKit
@testable import CurrencyTrading

final class TradeCurrencyViewMock: UIView, TradeCurrencyViewProtocol {

    var currencyTraded: String = ""

    var baseCurrency: CurrencyType? = .usa

    var didTapTradeButton: ((String) -> Void)?

    func setBaseCurrency(name: String) { }

    func setCurrencyToTrade(name: String) { }

    func setCurrencyTradedValue(with value: String) {
        currencyTraded = value
    }
}
