import XCTest
import OHHTTPStubs

class TradeCurrenciesFlowTests: XCTestCase {

    private let baseCurrency = "Dólar americano"
    private let errorBaseCurrency = "Iene japones"
    private let currencyToTrade = "Lev búlgaro"

    let app = XCUIApplication()

    override func setUp() {
        continueAfterFailure = false
        app.launchArguments = ["STUB_HTTP_ENDPOINTS"]
        app.launch()
    }

    override func tearDown() {
        app.terminate()
    }

    func testIfSelectCurrencyControllerPickersWorkingWell() {
        app.textFields["baseCurrencyPicker"].tap()
        app.pickerWheels.element.adjust(toPickerWheelValue: baseCurrency)
        app.textFields["currencyToTradePicker"].tap()
        app.pickerWheels.element.adjust(toPickerWheelValue: currencyToTrade)

        XCTAssertEqual(app.textFields["baseCurrencyPicker"].value as! String, baseCurrency)
        XCTAssertEqual(app.textFields["currencyToTradePicker"].value as! String, currencyToTrade)
    }

    func testIfSelectCurrencyControllerOpenTradeCurrencyView() {
        app.textFields["baseCurrencyPicker"].tap()
        app.pickerWheels.element.adjust(toPickerWheelValue: baseCurrency)
        app.textFields["currencyToTradePicker"].tap()
        app.pickerWheels.element.adjust(toPickerWheelValue: currencyToTrade)
        app.toolbars.element.buttons["donePickerButton"].tap()
        app.buttons["tradeCurrencyButton"].tap()

        let predicate = NSPredicate(format: "exists == 1")
        let tradeButton = app.buttons["tradeButton"]

        wait(for: [expectation(for: predicate, evaluatedWith: tradeButton, handler: nil)], timeout: 1.0)
    }

    func testIfSelectCurrencyControllerTreatErrorWell() {
        app.textFields["baseCurrencyPicker"].tap()
        app.pickerWheels.element.adjust(toPickerWheelValue: errorBaseCurrency)
        app.textFields["currencyToTradePicker"].tap()
        app.pickerWheels.element.adjust(toPickerWheelValue: currencyToTrade)
        app.toolbars.element.buttons["donePickerButton"].tap()
        app.buttons["tradeCurrencyButton"].tap()

        let predicate = NSPredicate(format: "exists == 1")
        let alert = app.alerts.staticTexts["Algo deu errado"]

        wait(for: [expectation(for: predicate, evaluatedWith: alert, handler: nil)], timeout: 1.0)
    }

    func testIfTradeCurrencyWorkWell() {
        app.textFields["baseCurrencyPicker"].tap()
        app.pickerWheels.element.adjust(toPickerWheelValue: baseCurrency)
        app.textFields["currencyToTradePicker"].tap()
        app.pickerWheels.element.adjust(toPickerWheelValue: currencyToTrade)
        app.toolbars.element.buttons["donePickerButton"].tap()
        app.buttons["tradeCurrencyButton"].tap()

        let predicate = NSPredicate(format: "exists == 1")
        let tradeButton = app.buttons["tradeButton"]

        wait(for: [expectation(for: predicate, evaluatedWith: tradeButton, handler: nil)], timeout: 3.0)

        let currencyToTradeField = app.otherElements["inputCurrencyValueField"]
        currencyToTradeField.tap()
        currencyToTradeField.typeText("1234")

        tradeButton.tap()

        XCTAssertEqual(app.staticTexts.element(matching: .any, identifier: "currencyToTradeValueLabel").label, "BGN 23.45")

    }
}
